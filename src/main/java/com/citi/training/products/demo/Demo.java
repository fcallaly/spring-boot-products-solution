package com.citi.training.products.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.products.Product;
import com.citi.training.products.repository.ProductRepository;

@Component
@Profile("demo")
public class Demo implements ApplicationRunner{

    private static Logger logger = LoggerFactory.getLogger(Demo.class);

    @Autowired
    private ProductRepository productRepository;

    public void run(ApplicationArguments appArgs) {
        productRepository.saveProduct(new Product(0, "Beans", 99.99));
        productRepository.saveProduct(new Product(1, "Ham", 5.99));

        printAllProducts();

        int idToRemove = 1;
        logger.info("Removing product: " +
                    productRepository.getProduct(idToRemove));
        productRepository.removeProduct(idToRemove);

        printAllProducts();
    }

    public void printAllProducts() {
        logger.info("All products currently in repository:");
        for(Product product : productRepository.getAllProducts()) {
            logger.info(product.toString());
        }
    }
}
