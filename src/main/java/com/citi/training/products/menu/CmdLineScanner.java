package com.citi.training.products.menu;

import java.util.Map;
import java.util.Scanner;

import org.springframework.stereotype.Component;

// This class comprises static helper methods.
@Component
public class CmdLineScanner implements InputReader {

    // Create a Scanner object, to get keyboard input.
    private Scanner scanner = new Scanner(System.in);

    // Get a String from the user.
    public String getString(String promptMsg) {
        System.out.printf("%s", promptMsg);
        return scanner.next();
    }

    // Get a double from the user.
    public double getDouble(String promptMsg) {
        System.out.printf("%s", promptMsg);
        return scanner.nextDouble();
    }

    // Get an int from the user.
    public int getInt(String promptMsg) {
        System.out.printf("%s", promptMsg);
        return scanner.nextInt();
    }

    // Generic method, displays all the items in a Collection<T>.
    public <K, V> void displayCollection(Map<K, V> map) {
        for (V element : map.values()) {
            System.out.printf("%s.\n", element);
        }
    }
}
