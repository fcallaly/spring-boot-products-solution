package com.citi.training.products.menu;

public interface InputReader {
    String getString(String promptMsg);
    double getDouble(String promptMsg);
    int getInt(String promptMsg);
}
