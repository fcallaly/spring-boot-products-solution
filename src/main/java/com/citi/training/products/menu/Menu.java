package com.citi.training.products.menu;

import java.util.InputMismatchException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.products.Product;
import com.citi.training.products.repository.ProductRepository;

@Component
@Profile("default")
public class Menu implements ApplicationRunner {

    Logger logger = LoggerFactory.getLogger(Menu.class);

    @Autowired
    InputReader inputReader;

    @Autowired
    ProductRepository productRepository;

    public void run(ApplicationArguments appArgs) {

        logger.info("Menu is starting up");

        int id;
        String name;
        double price;
        Product product;
        AtomicInteger idGenerator = new AtomicInteger(0);

        // Display menu options in a loop.
        int option = -1;
        do {
            System.out.println();
            System.out.println("---------------------------------------------------------");
            System.out.println("1. Create Product");
            System.out.println("2. Remove Product");
            System.out.println("3. Print a Product");
            System.out.println("4. Print all Products");
            System.out.println("5. Quit");

            try {
                option = inputReader.getInt("\nEnter option => ");

                switch (option) {

                case 1:
                    name = inputReader.getString("\nEnter product name: ");
                    price = inputReader.getDouble("\nEnter product price: ");
                    product = new Product(idGenerator.getAndAdd(1), name, price);
                    productRepository.saveProduct(product);
                    System.out.println("Created: " + product + "\n");
                    break;

                case 2:
                    id = inputReader.getInt("Enter id: ");
                    product = productRepository.removeProduct(id);
                    if(product != null) {
                        System.out.println("Item removed: " + product);
                    } else {
                        System.out.println("No item found");
                    }
                    break;

                case 3:
                    id = inputReader.getInt("Enter id: ");
                    product = productRepository.getProduct(id);
                    if(product != null) {
                        System.out.printf("Product details: %s.\n", product);
                    } else {
                        System.out.printf("No product with id %s.\n", id);
                    }
                    break;

                case 4:
                    System.out.println("All products");
                    for(Product thisProduct : productRepository.getAllProducts()) {
                        System.out.println(thisProduct);
                    }
                    break;

                case 5:
                    // This is a valid option, but there's nothing to do here.
                    break;

                default:
                    System.out.println("Invalid option selected.");
                }
            } catch (InputMismatchException ex) {
                logger.error("User Entered invalid option - Exiting");
                System.out.println("Invalid option selected - Exiting");
                option = 5;
            }
        } while (option != 5);
    }
}
