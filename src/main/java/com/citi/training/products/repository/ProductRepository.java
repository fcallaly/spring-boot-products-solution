package com.citi.training.products.repository;

import java.util.List;

import com.citi.training.products.Product;

public interface ProductRepository {

    void saveProduct(Product product);
    Product getProduct(int id);
    List<Product> getAllProducts();
    Product removeProduct(int id);
}
