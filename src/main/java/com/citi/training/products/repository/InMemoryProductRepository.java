package com.citi.training.products.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.citi.training.products.Product;

@Component
public class InMemoryProductRepository implements ProductRepository {

    private HashMap<Integer, Product> allProducts =
                                        new HashMap<Integer, Product>();

    public void saveProduct(Product product) {
        allProducts.put(product.getId(), product);
    }

    public Product getProduct(int id) {
        return allProducts.get(id);
    }

    public List<Product> getAllProducts(){
        return new ArrayList<Product>(allProducts.values());
    }

    public Product removeProduct(int id) {
        return allProducts.remove(id);
    }
}
