package com.citi.training.products.repository;


import org.junit.Before;
import org.junit.Test;

import com.citi.training.products.Product;

public class InMemoryProductRepositoryTests {

    private InMemoryProductRepository inMemProdRepo;

    @Before
    public void setup() {
        inMemProdRepo = new InMemoryProductRepository();
        inMemProdRepo.saveProduct(new Product(0, "Beans", 99.99));
        inMemProdRepo.saveProduct(new Product(1, "Ham", 5.99));
    }

    @Test
    public void test_InMemProdRepository_save() {
        Product testProduct = new Product(2,  "Jam", 9.99);
        inMemProdRepo.saveProduct(testProduct);
        assert(inMemProdRepo.getProduct(2) == testProduct);
    }

    @Test
    public void test_InMemProdRepository_getAllProducts() {
        assert(inMemProdRepo.getAllProducts().size() == 2);
    }

    @Test
    public void test_InMemProdRepository_remove() {
        assert(inMemProdRepo.removeProduct(0).getName().equals("Beans"));
        assert(inMemProdRepo.getAllProducts().size() == 1);
    }

}
