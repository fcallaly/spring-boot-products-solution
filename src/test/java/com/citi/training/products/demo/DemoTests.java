package com.citi.training.products.demo;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.Product;
import com.citi.training.products.repository.ProductRepository;

@ActiveProfiles("demo")
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoTests {

    // A "fake" productRepository
    @SpyBean
    private ProductRepository productRepository;

    // Note: A "real" Demo Bean will be created by spring,
    // and it's run method called

    @Test
    public void test_Demo_run() {
        // verify productRepository.saveProduct is called twice
        verify(productRepository, times(2)).saveProduct(
                                Mockito.any(Product.class));
    }

}
